# profile::demo::postgresql
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include profile::demo::postgresql
class profile::demo::postgresql (
  String $user_name = 'root',
  String $user_password = undef,
) {
  class { 'postgresql::server':
   ip_mask_deny_postgres_user => '0.0.0.0/32',
   ip_mask_allow_all_users    => '0.0.0.0/0',
   ipv4acls                   => [
     'local all postgres trust',
     "local all $user_name trust",
     "hostssl all $user_name 0.0.0.0/0 password",
   ],
  }

  postgresql::server::role { $user_name: 
    ensure        => 'present',
    password_hash => postgresql_password($user_name, $user_password),
  }
}
