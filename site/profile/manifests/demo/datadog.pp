# profile::demo::datadog
#
# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include profile::demo::datadog
class profile::demo::datadog (
  String $api_key,
  Boolean $run_reports = false,
) {
  class { "datadog_agent":
    api_key => $api_key,
    puppet_run_reports => $run_reports
  }
}
